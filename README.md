Augh Talk
=======
![](website/static/aughtalk_icon.png)<br/>
Il progetto si propone di realizzare un’applicazione gratuita e open source per smartphone e tablet Android, per migliorare e semplificare la comunicazione aumentativa utilizzata da persone affette da autismo e disturbi correlati. Tale progetto è inquadrato in un contesto di alternanza scuola lavoro (ASL) con la cooperativa Onlus Solaris  (CDD - Centro Diurno per Disabili di Macherio). Il progetto di quest’anno è un’evoluzione di un analogo progetto iniziato l’anno scorso con la classe 4IA 2016/2017 con la stessa Solaris.
La comunicazione aumentativa per immagini utilizza simboli grafici e/o immagini su tessere per rappresentare concetti e definire un canale di comunicazione. L’app “AughTalk” permette di usare il sensore NFC presente su alcuni modelli di smartphone e tablet per riconoscere i tag NFC associati alle tessere in modo tale che quando la tessera con il relativo tag NFC è avvicinato al dispositivo l’applicazione è in grado di riconoscere la tessera e di emettere un suono o di riprodurre una parola o frase che richiama il concetto associato alla tessera.

Il progetto è stato svolto dagli studenti della classe 4IB a.s. 2017/2018, dell’indirizzo Informatica e Telecomunicazioni (articolazione informatica) dell’IISS A. Greppi di Monticello Brianza nell’ambito del piano di alternanza scuola lavoro previsto dalla riforma della “Buona Scuola”, con la supervisione del docente di Informatica Prof. Gennaro Malafronte e dell’ITP Luca Melcarne.
L’attività di ASL è stata svolta in modalità "Project Work", sia durante le ore curricolari di Informatica, sia in ore aggiuntive pomeridiane nei laboratori della scuola, sempre con la supervisione dei docenti di informatica.

Il nome AughTalk deriva dalla parola indiana "augh" che vuol dire saluto ma che si pronuncia anche come la prima parte di "augmented" e "talk" che vuol dire parlare. Quindi la parola AughTalk richiama sia il concetto di saluto che quello di comunicazione aumentativa.

Applicazione Android
--------------------

[![link per l'installazione di AughTalk](website/static/AughTalkIntro.png)]( https://play.google.com/store/apps/details?id=it.issgreppi.aughtalk "AughTalk Download")
Download
--------
Attenzione: l'applicazione richiede l'uso di un telefono provvisto di sensore NFC e della disponibilità di tag NFC per poter programmare azioni da eseguire.
## Download dell'[apk dal Google Play Store][2]
[![link per l'installazione di AughTalk](website/static/google-play-badge.png)]( https://play.google.com/store/apps/details?id=it.issgreppi.aughtalk "AughTalk Download")

## Video
[![Video dello spot di AughTalk](website/static/VideoSpotAughTalkImage.png)](https://youtu.be/Lv_xiFYL4_8 "AughTalk Spot")
<br/>AughTalk - the spot

[![Video del backstage del progetto AughTalk](website/static/BackStageVideoAughTalkImge.png)](https://youtu.be/RM39J4ezNEE "AughTalk BackStage")
<br/>AughTalk - the backstage

## Screenshots
![](website/static/AughTalkDrawer.png)
![](website/static/AughTalkCategories.jpg)
![](website/static/AughTalkAddItem.png)
![](website/static/AughTalkItems.jpg)

-------------------------------
La scheda descrittiva del progetto è scaricabile a questo [link][3]

Per ulteriori informazioni sul progetto contattare l'[Istituto Alessandro Greppi][1] di Monticello Brianza (Lc)




Licenza
--------
    Copyright 2018 IISS "Alessandro Greppi" - Monticello Brianza (Lc),
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
       http://www.apache.org/licenses/LICENSE-2.0
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
 [1]: http://www.issgreppi.gov.it
 [2]: https://play.google.com/store/apps/details?id=it.issgreppi.aughtalk
 [3]: https://gitlab.com/GreppiShare/AughTalkBuild/raw/master/website/static/Progetto_comunicatore_per_immagini-AughTalk.pdf
